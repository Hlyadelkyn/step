"use strict";

// SERVICE SECTION

let tabButtons = document.querySelectorAll(".service-tab button");
let tabButtonIcons = document.querySelectorAll(".tab-marker");
let tabElems = document.querySelectorAll(".service-tab-content");

function changeTab(index) {
  tabButtons.forEach(function (item) {
    item.style.backgroundColor = "#f8fcfe";
    item.style.color = "#717171";
  });
  tabButtonIcons.forEach(function (item) {
    item.style.display = "none";
  });
  tabButtons[index].style.backgroundColor = "#18CFAB";
  tabButtons[index].style.color = "#FFFFFF";
  tabButtonIcons[index].style.display = "block";
  tabElems.forEach(function (item) {
    item.style.display = "none";
  });
  tabElems[index].style.display = "flex";
}
changeTab(0);

// OUR WORK
function loadWorkImgs() {
  let WorkImgsListWrapper = document.querySelector(".work-images-wrapper");

  WorkImgsListWrapper.insertAdjacentHTML(
    "beforeend",
    `<li class="images-list-item graphic-design active">
  <img
    src="./img/graphic design/graphic-design1.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">Graphic design</p>
  </div>
</li>
<li class="images-list-item web-design active">
  <img
    src="./img/web design/web-design1.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">Web design</p>
  </div>
</li>
<li class="images-list-item landing-page active">
  <img
    src="./img/landing page/landing-page1.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">landing page</p>
  </div>
</li>
<li class="images-list-item landing-page active">
  <img
    src="./img/landing page/landing-page2.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">landing page</p>
  </div>
</li>
<li class="images-list-item wordpress active">
  <img
    src="./img/wordpress/wordpress1.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">Wordpress</p>
  </div>
</li>
<li class="images-list-item web-design active">
  <img
    src="./img/web design/web-design2.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">web design</p>
  </div>
</li>
<li class="images-list-item graphic-design active">
  <img
    src="./img/graphic design/graphic-design2.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">Graphic design</p>
  </div>
</li>
<li class="images-list-item graphic-design active">
  <img
    src="./img/graphic design/graphic-design3.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">Graphic design</p>
  </div>
</li>
<li class="images-list-item web-design active">
  <img
    src="./img/web design/web-design3.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">web design</p>
  </div>
</li>
<li class="images-list-item wordpress active">
  <img
    src="./img/wordpress/wordpress2.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">wordpress</p>
  </div>
</li>
<li class="images-list-item landing-page active">
  <img
    src="./img/landing page/landing-page3.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">landing page</p>
  </div>
</li>
<li class="images-list-item wordpress active">
  <img
    src="./img/wordpress/wordpress3.jpg"
    alt="graphic design icon"
    width="285"
    height="211"
  />
  <div class="images-list-item-description">
    <div class="imgs-li-icons-wrapper">
      <img src="./img/work-icons/1.svg" class="imgs-li-icon" />
      <a href="#">
        <div class="imags-li-shape">
          <div class="primary"></div>
          <div class="secondary"></div></div
      ></a>
    </div>
    <h4 class="imgs-item-title">Creative Desing</h4>
    <p class="imgs-item-desc">wordpress</p>
  </div>
</li>`
  );
}

function filterOurWorkImgs(typeToShow) {
  let imgsItemList = document.querySelectorAll(".images-list-item");
  let imgsButnsList = document.querySelectorAll(".our-work-button");
  if (typeToShow === 0) {
    imgsItemList.forEach((item) => item.classList.add("active"));
    imgsButnsList.forEach((item) => item.classList.remove("active"));
    imgsButnsList[0].classList.add("active");
  } else if (typeToShow === 1) {
    imgsItemList.forEach((item) => item.classList.remove("active"));
    imgsItemList.forEach(function (elem) {
      if (elem.classList.contains("graphic-design")) {
        elem.classList.add("active");
      }
    });
    imgsButnsList.forEach((item) => item.classList.remove("active"));
    imgsButnsList[1].classList.add("active");
  } else if (typeToShow === 2) {
    imgsItemList.forEach((item) => item.classList.remove("active"));
    imgsItemList.forEach(function (elem) {
      if (elem.classList.contains("web-design")) {
        elem.classList.add("active");
      }
    });
    imgsButnsList.forEach((item) => item.classList.remove("active"));
    imgsButnsList[2].classList.add("active");
  } else if (typeToShow === 3) {
    imgsItemList.forEach((item) => item.classList.remove("active"));
    imgsItemList.forEach(function (elem) {
      if (elem.classList.contains("landing-page")) {
        elem.classList.add("active");
      }
    });
    imgsButnsList.forEach((item) => item.classList.remove("active"));
    imgsButnsList[3].classList.add("active");
  } else if (typeToShow === 4) {
    imgsItemList.forEach((item) => item.classList.remove("active"));
    imgsItemList.forEach(function (elem) {
      if (elem.classList.contains("wordpress")) {
        elem.classList.add("active");
      }
    });
    imgsButnsList.forEach((item) => item.classList.remove("active"));
    imgsButnsList[4].classList.add("active");
  }
}

// SLIDER
const left = document.querySelector(".arrow.left");
const right = document.querySelector(".arrow.right");

const slider = document.querySelector(".reviews-slider");

const indicatorParent = document.querySelector(".slider-nav");
const indicators = document.querySelectorAll(".slider-nav-item");

const sliderIcons = document.querySelectorAll(".reviewer-photo-container");
let index = 0;

indicators.forEach((indicator, i) => {
  indicator.addEventListener("click", () => {
    document
      .querySelector(".slider-nav-item.selected")
      .classList.remove("selected");

    indicator.classList.add("selected");

    sliderIcons.forEach((item) => item.classList.remove("active"));
    sliderIcons[i].classList.add("active");

    slider.style.transform = "translateX(" + i * -25 + "%)";
    index = i;
  });
});

left.addEventListener("click", function () {
  index = index > 0 ? index - 1 : 3;
  document
    .querySelector(".slider-nav-item.selected")
    .classList.remove("selected");
  indicatorParent.children[index].classList.add("selected");

  sliderIcons.forEach((item) => item.classList.remove("active"));
  sliderIcons[index].classList.add("active");

  slider.style.transform = "translateX(" + index * -25 + "%)";
});

right.addEventListener("click", function () {
  index = index < 4 - 1 ? index + 1 : 0;
  document
    .querySelector(".slider-nav-item.selected")
    .classList.remove("selected");
  indicatorParent.children[index].classList.add("selected");

  sliderIcons.forEach((item) => item.classList.remove("active"));
  sliderIcons[index].classList.add("active");

  slider.style.transform = "translateX(" + index * -25 + "%)";
});

// GALERY SECTION
let loadImgsCount = 0;
function loadImgs() {
  loadImgsCount += 1;

  let imgsList = document.querySelectorAll(".gallery-item");
  let gallery = document.querySelector(".gallery-container");
  if (loadImgsCount === 1) {
    gallery.style.maxHeight = "1400px";
    imgsList[3].classList.add("active");
    imgsList[9].classList.add("active");
    imgsList[15].classList.add("active");
  } else if (loadImgsCount === 2) {
    gallery.style.maxHeight = "1680px";
    imgsList[4].classList.add("active");
    imgsList[10].classList.add("active");
    imgsList[16].classList.add("active");
  } else {
    gallery.style.maxHeight = "1980px";
    imgsList[5].classList.add("active");
    imgsList[11].classList.add("active");
    imgsList[17].classList.add("active");
    let buttonToRemove = document.querySelector(".gallery-button");
    buttonToRemove.style.display = "none";
  }
}

// SCROLL TO TOP

let btnToTop = document.querySelector(".scroll-top-top-btn");

function btnVisible() {
  if (document.documentElement.scrollTop < 992) {
    btnToTop.style.display = "none";
  } else {
    btnToTop.style.display = "flex  ";
  }
}
btnVisible();

document.addEventListener("scroll", function () {
  btnVisible();
});

btnToTop.addEventListener("click", function () {
  window.scrollTo({
    top: 0,
    left: 0,
    behavior: "smooth",
  });
});
